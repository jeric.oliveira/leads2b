# Container Definition
data "template_file" "container_definitions_b" {
  template = file("${path.module}/templates/container-definitions-b.json")
}

# Task Definition
resource "aws_ecs_task_definition" "leads2b_b" {
  family = "leads2b-b"

  execution_role_arn = aws_iam_role.ecs_task_execution.arn

  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  cpu = 256
  memory = 512

  container_definitions = data.template_file.container_definitions_b.rendered
}

# Service
resource "aws_ecs_service" "leads2b_b" {
  name = "leads2b-b"

  cluster = aws_ecs_cluster.leads2b.arn

  task_definition = aws_ecs_task_definition.leads2b_b.arn
  desired_count = 1
  launch_type = "FARGATE"
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent = 200

  network_configuration {
    security_groups = [module.leads2b_ecs_service_security_group.this_security_group_id]
    subnets = module.vpc.private_subnets
  }

  load_balancer {
    target_group_arn = element(module.load_balancer.target_group_arns, 1)
    container_name = "nginx"
    container_port = 80
  }

  lifecycle {
    create_before_destroy = true
    #Permit changes in interface AWS (https://www.terraform.io/docs/configuration/resources.html#ignore_changes)
    ignore_changes = [task_definition, desired_count]
  }
}
