# Container Definition
data "template_file" "container_definitions_a" {
  template = file("${path.module}/templates/container-definitions-a.json")
}

# Task DefinitioN
resource "aws_ecs_task_definition" "leads2b_a" {
  family = "leads2b-a"

  execution_role_arn = aws_iam_role.ecs_task_execution.arn

  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  cpu = 256
  memory = 512

  container_definitions = data.template_file.container_definitions_a.rendered
}

# Service
resource "aws_ecs_service" "leads2b_a" {
  name = "leads2b-a"

  cluster = aws_ecs_cluster.leads2b.arn

  task_definition = aws_ecs_task_definition.leads2b_a.arn
  desired_count = 2
  launch_type = "FARGATE"
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent = 200

  network_configuration {
    security_groups = [module.leads2b_ecs_service_security_group.this_security_group_id]
    subnets = module.vpc.private_subnets
  }

  load_balancer {
    target_group_arn = element(module.load_balancer.target_group_arns, 0)
    container_name = "httpd"
    container_port = 80
  }

  lifecycle {
    create_before_destroy = true
    #Permit changes in interface AWS (https://www.terraform.io/docs/configuration/resources.html#ignore_changes)
    ignore_changes = [task_definition, desired_count]
  }
}
