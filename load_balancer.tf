module "load_balancer" {
  source = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = "leads2b-alb"

  vpc_id = module.vpc.vpc_id
  subnets = module.vpc.public_subnets
  security_groups = [module.load_balancer_security_group.this_security_group_id]

  target_groups = [
    {
      name = "leads2b-a-http"
      backend_protocol = "HTTP"
      backend_port = 80
      target_type = "ip"
      deregistration_delay = 10
      health_check = {
        enabled = true
        interval = 30
        path = "/"
        port = "traffic-port"
        healthy_threshold = 3
        unhealthy_threshold = 3
        timeout = 6
        protocol = "HTTP"
        matcher = "200-399"
      }
    },
    {
      name = "leads2b-b-http"
      backend_protocol = "HTTP"
      backend_port = 80
      target_type = "ip"
      deregistration_delay = 10
      health_check = {
        enabled = true
        interval = 30
        path = "/"
        port = "traffic-port"
        healthy_threshold = 3
        unhealthy_threshold = 3
        timeout = 6
        protocol = "HTTP"
        matcher = "200-399"
      }
    }
  ]

  http_tcp_listeners = [{
    port = 80
    protocol = "HTTP"
  }]
}

resource "aws_lb_listener_rule" "leads2b_b" {
  listener_arn = element(module.load_balancer.http_tcp_listener_arns, 0)
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = element(module.load_balancer.target_group_arns, 1)
  }

  condition {
    path_pattern {
      values = ["/nginx/*"]
    }
  }
}
