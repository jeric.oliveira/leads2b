output "load_balancer_url" {
  value = module.load_balancer.this_lb_dns_name
}
