resource "aws_cloudwatch_log_group" "leads2b_httpd" {
  name = "/ecs/leads2b/httpd"
  retention_in_days = 3
}

resource "aws_cloudwatch_log_group" "leads2b_nginx" {
  name = "/ecs/leads2b/nginx"
  retention_in_days = 3
}
