module "leads2b_ecs_service_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/http-80"
  version = "~> 3.0"

  name = "leads2b-ecs-service"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["100.0.0.0/16"]
}

module "load_balancer_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/http-80"
  version = "~> 3.0"

  name = "leads2b-alb"
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}
